const hostname = "localhost";
const port = 8080;
const express = require('express');
const app = express();
const fs = require('fs');
const path = require("path");
const jsonParser = express.json();

let createNewFile = (req, res) => {
    if (!req.body.content)
        return res
            .status(400)
            .send({ message: "Please specify 'content' parameter" });

    if (!req.body.filename)
        return res
            .status(400)
            .send({ message: "Please specify 'filename' parameter" });

    const pathFile = `files/${req.body.filename}`;

    if (!fs.existsSync("./files")) {
        fs.mkdirSync("./files");
    }

    if (!fs.existsSync(pathFile)) {
        fs.appendFileSync(pathFile, req.body.content);
        res.status(200).send({ message: "File created successfully" });
    }
}
let readFile = (req, res) => {
    const listFiles = fs.readdirSync('./files');
    const result = listFiles.map(file => {
        return `"${file}"`;
    });

    if (listFiles.length) {
        res.status(200).send({
            message: "Success",
            files: [`${result}`],
        });
    } else {
        res.status(400).send({ message: "Server error" });
    }
}


app.get('/api/files/:filename', function(req, res) {
    console.log(res.send(JSON.stringify(req.params)));
});
app.get('/api/files', readFile);

app.post('/api/files/', jsonParser, createNewFile);

app.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});